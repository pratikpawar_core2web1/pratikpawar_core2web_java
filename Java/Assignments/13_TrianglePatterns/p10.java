
import java.util.*;
class pdemo{
	
	public static void main(String arg[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows = ");
		int rows = sc.nextInt();
		int temp = rows;

		for(int i=1;i<=rows;i++,temp--){
			
			int ch1 = temp+64;
			int ch2 = temp+96;
			for(int j=1;j<=temp;j++){
				
				if(temp%2==1){
					System.out.print((char)ch2-- + " ");
			
				}else{
					System.out.print((char)ch1-- + " ");
					
				}
				
			}
			System.out.println();
		}
	}
}
