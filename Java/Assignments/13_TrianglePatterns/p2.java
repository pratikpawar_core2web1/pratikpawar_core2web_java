
import java.util.*;
class pdemo{
	
	public static void main(String arg[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows = ");
		int rows = sc.nextInt();
		int temp = rows;
		
		int n = 2;
		int m = 1;
		for(int i=1;i<=rows;i++){
			
			for(int j=temp;j>=i;j--){
				
				System.out.print(n*m++ + " ");
			}
			System.out.println();
		}
	}
}
