
import java.util.*;
class pdemo{
	
	public static void main(String arg[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows = ");
		int rows = sc.nextInt();
		int temp = rows;
		
		int n = temp*(temp+1);
		for(int i=1;i<=rows;i++){
			
			for(int j=temp;j>=i;j--){
				
				System.out.print(n + " ");
				n-=2;
			}
			System.out.println();
		}
	}
}
