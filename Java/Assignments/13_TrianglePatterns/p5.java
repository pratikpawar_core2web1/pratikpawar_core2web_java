
import java.util.*;
class pdemo{
	
	public static void main(String arg[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows = ");
		int rows = sc.nextInt();

		for(int i=1;i<=rows;i++){
			char ch1='A';
			char ch2 = 'a';
			for(int j=rows;j>=i;j--){
				
				if(i%2==0){
					System.out.print(ch2++ + " ");
			
				}else{
					System.out.print(ch1++ + " ");
					
				}
			}
			System.out.println();
		}
	}
}
