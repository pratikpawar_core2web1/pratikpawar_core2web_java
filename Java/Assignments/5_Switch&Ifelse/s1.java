

class sdemo{

	public static void main(String ar[]){
		
		int n =-57;

		switch(n%2){
			
			case 0:
				System.out.println(n+" is even");
				break;

			case 1:
			case -1:
				System.out.println(n+" is odd");
				break;
			
			default : 
				System.out.println("Invalid input");
		}
	}
}
