

class sdemo{
	
	public static void main(String sar[]){
	
		int bgt = 6000;
		
		switch(bgt){
			
			case 15000: 
				System.out.println("For budget "+bgt+" destination is Jammu and Kashmir");
				break;

			case 10000: 
				System.out.println("For budget "+bgt+" destination is Manali");
				break;

			case 6000: 
				System.out.println("For budget "+bgt+" destination is Amritsar");
				break;
			
			case 2000: 
				System.out.println("For budget "+bgt+" destination is Mahabaleshwar");
				break;
		
			default: 
				System.out.println("For Other budgets try next time");
				break;
		}
	}
}
