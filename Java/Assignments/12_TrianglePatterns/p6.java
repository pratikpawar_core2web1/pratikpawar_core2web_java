
import java.io.*;

class pdemo{
	
	public static void main(String arf[])throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter rows = ");
		int rows = Integer.parseInt(br.readLine());
		
		char ch2 = 'A';
		for(int i=1;i<=rows;i++){
			
			int ch1 = 1;
			for(int j=1;j<=i;j++){
				
				if(i%2==1){
					System.out.print(ch1++ + " ");
				
				}
				else{
					System.out.print(ch2 + " ");
				}
				ch2++;
			}
			System.out.println();
		}	

	}
}
