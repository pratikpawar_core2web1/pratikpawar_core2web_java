
import java.io.*;

class pdemo{
	
	public static void main(String arf[])throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter rows = ");
		int rows = Integer.parseInt(br.readLine());
		
		for(int i=1;i<=rows;i++){
			
			int ch1 = rows+64;
			int ch2 = rows+96;
			for(int j=1;j<=i;j++){
				
				if(i%2==0){
					System.out.print((char)ch1-- + " ");
				}
				else{
					System.out.print((char)ch2-- + " ");
				}
			}
			System.out.println();
		}	

	}
}
