

class sdemo{
	
	public static void main(String arg[]){
	
		char data = 'P';

		System.out.println("Before switch");

		switch(data){
			
			case 'A': 
				System.out.println("A");
				break;
			case 'B': 
				System.out.println("Two");
				break;
			case 'P':
				System.out.println("P");
				break;
			default : 
				System.out.println("In default state");
		}

			System.out.println("After switch");

	}
}
