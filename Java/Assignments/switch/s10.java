

class sdemo{
	
	public static void main(String arg[]){
	
		char data = 'A';

		System.out.println("Before switch");

		switch(data){
			
			case 'A': 
				System.out.println("A");
				break;
			case 65 : 
				System.out.println("65");
				break;
			case 'B': 
				System.out.println("B");
				break;
			case 66 : 
				System.out.println("66");
				break;
			case 'P':
				System.out.println("P");
				break;
			default : 
				System.out.println("In default state");
		}

			System.out.println("After switch");

	}
}
