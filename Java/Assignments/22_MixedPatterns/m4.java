
import java.util.*;

class mdemo{
	
	public static void main(String argp[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows = ");
		int rows = sc.nextInt();
		
		int n=1;
		int cnt=0;
		for(int i=1;i<=rows;i++){
			char ch1='A';
			char ch2='a';

			for(int s=1;s<i;s++){
				
				System.out.print("\t");
				ch1++;
				ch2++;
			}
			for(int j=rows;j>=i;j--){
				
				if(rows % 2==1){
					System.out.print(ch1++ + "\t");
				}
				else{
					System.out.print(ch2++ + "\t");
				}
			}

			n = cnt;
			System.out.println();
		}
	}
}
