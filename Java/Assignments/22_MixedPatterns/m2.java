
import java.util.*;

class mdemo{
	
	public static void main(String argp[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows = ");
		int rows = sc.nextInt();
		
		int n=1;
		int cnt=0;
		for(int i=1;i<=rows;i++){
			
			for(int s=1;s<i;s++){
				
				System.out.print("\t");
			}
			
			for(int j=rows;j>=i;j--){
				cnt =n;
				System.out.print(n++ + "\t");
			}

			n = cnt;
			System.out.println();
		}
	}
}
