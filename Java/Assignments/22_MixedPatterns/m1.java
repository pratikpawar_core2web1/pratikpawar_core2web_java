
import java.util.*;

class mdemo{
	
	public static void main(String argp[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows = ");
		int rows = sc.nextInt();
		
		int n=1;
		for(int i=1;i<=rows;i++){
			
			for(int s=rows;s>i;s--){
				
				System.out.print("\t");
			}
			for(int j=1;j<=i;j++){
				
				System.out.print(n + "\t");
				n+=2;
			}
			System.out.println();
		}
	}
}
