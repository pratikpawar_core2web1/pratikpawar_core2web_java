
import java.util.*;

class mdemo{
	
	public static void main(String argp[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows = ");
		int rows = sc.nextInt();
		
		int n=3;
		for(int i=1;i<=rows;i++){
			
			for(int j=1;j<=rows;j++){
				
				if(i%2==1){
					System.out.print(n*n++ + "\t");
				}
				else{
					System.out.print(n++ + "\t");
				}
			}
			System.out.println();
		}
	}
}
