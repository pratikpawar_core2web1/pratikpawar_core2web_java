
import java.util.*;

class mdemo{
	
	public static void main(String argp[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows = ");
		int rows = sc.nextInt();
		
		
		for(int i=1;i<=rows;i++){
			int n=i*2-1;
			for(int s=rows;s>i;s--){
				
				System.out.print("\t");
			}
			for(int j=1;j<=i*2-1;j++){
				
				System.out.print(n-- + "\t");
				
			}
			System.out.println();
		}
	}
}
