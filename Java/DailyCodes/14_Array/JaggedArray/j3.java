
import java.util.*;
class jdemo{
	
	public static void main(String arg[]){
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows and cols  : ");
		int rows = sc.nextInt();
		int cols = sc.nextInt();

		int arr[][] = new int[rows][cols];
	
		System.out.println("Enter Array elements: ");
		
		for(int i=0;i<arr.length ;i++){

			for(int j=0;j<arr[i].length;j++){
			
				arr[i][j]= sc.nextInt();
			}
		}
		System.out.println("Array elements are : ");
		for(int i=0;i<arr.length ;i++){
		
			for(int j=0;j<arr[i].length;j++){
			
				System.out.print(arr[i][j]+ " ");
			}
			System.out.println();
		}
		
		System.out.println(arr[1][1]);// 5
		System.out.println(arr[1]); // [I@dfjhk
		System.out.println(arr);  // [[I@sdfgj

	}
}
