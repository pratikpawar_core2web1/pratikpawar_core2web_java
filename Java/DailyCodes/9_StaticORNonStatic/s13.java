

class sdemo{
	
	int x = 30;
	static int y = 40;
	
	void fun(){
		
		System.out.println("In fun function");
	}
	static void run(){
		
		System.out.println("In run function");
	}

	public static void main(String ar[]){
		
		sdemo obj = new sdemo();
		System.out.println(obj.x);
		System.out.println(obj.y);
		obj.fun();
		obj.run();
	}
}
