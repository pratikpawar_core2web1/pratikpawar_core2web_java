
import java.io.*;
class pdemo{


	public static void main(String arg[])throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter rows : ");
		int rows = Integer.parseInt(br.readLine());
		
		for (int i=1;i<=rows;i++){
			
			for(int j=1; j<=i;j++){
				
				System.out.print("* ");
			}
			System.out.println();
		}
	}
}
