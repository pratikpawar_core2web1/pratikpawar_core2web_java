
import java.util.*;
class pdemo{
	
	public static void main(String arg[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows = ");
		int rows = sc.nextInt();
		int temp = rows;
		for(int i=1;i<=rows;i++,temp--){
			int  n=1;
			char ch1 = 'a';
			for(int j=1;j<=temp;j++){
				
				if(j%2==0){
					System.out.print(ch1++ + " ");
			
				}else{
					System.out.print(n++ + " ");
					
				}
			}
			System.out.println();
		}
	}
}
