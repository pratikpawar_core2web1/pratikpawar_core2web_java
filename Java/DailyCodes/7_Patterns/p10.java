
import java.io.*;

class pdemo{
	
	public static void main(String arf[])throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter rows = ");
		int rows = Integer.parseInt(br.readLine());
		
		int n =1;
		char ch = 'a';
		for(int i=1;i<=rows;i++){
			
			for(int j=1;j<=i;j++){
				
				if(i%2==0){
					
					System.out.print(ch +  " ");
				}	
				else{
					
					System.out.print(n + " ");
						
				}
				n++;
				ch++;
			}
			System.out.println();
		}	

	}
}
