
import java.util.*;
class pdemo{
	
	public static void main(String arg[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows = ");
		int rows = sc.nextInt();

		for(int i=1;i<=rows;i++){
			int n =i;
			for(int j=rows;j>=i;j--){
				
				System.out.print(n++ + " ");
			}
			System.out.println();
		}
	}
}
