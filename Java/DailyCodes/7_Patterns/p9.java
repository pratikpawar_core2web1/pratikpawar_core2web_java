
import java.io.*;

class pdemo{
	
	public static void main(String arf[])throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter rows = ");
		int rows = Integer.parseInt(br.readLine());
		
		char ch = 'a';
		for(int i=1;i<=rows;i++){
			
			int n = rows +1;
			for(int j=1;j<=i;j++){
				
				if(j%2==0){
					
					System.out.print(ch++ +  " ");
				}	
				else{
					
					System.out.print(n + " ");
						
				}
				n++;
			}
			System.out.println();
		}	

	}
}
