
import java.util.*;
class pdemo{
	
	public static void main(String arg[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows = ");
		int rows = sc.nextInt();
		int temp = rows;

		for(int i=1;i<=rows;i++,temp--){
			
			int n =temp;
			for(int j=temp;j>=1;j--){
				
				if(i%2==0){
					int ch =j+64;
					System.out.print((char)ch-- + " ");
			
				}else{
					System.out.print(n-- + " ");
					
				}
				
			}
			System.out.println();
		}
	}
}
