
// &&,||,!
class Logical{

	public static void main(String a[]){
	
		boolean x,y;
		x = true;
		y = false;

		System.out.println(x&&y);	//false
		System.out.println(x||y);	//true
		System.out.println(!x);		//false
		System.out.println(!y);		//true
	}
}
