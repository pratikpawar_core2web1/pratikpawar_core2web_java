
// &,|,^,~,<<,>>,>>>
class Bitwise{

	public static void main(String a[]){
	
		int x=10;
		int y = 12;

		System.out.println(x&y);	 //8
		System.out.println(x|y);	//14

		System.out.println(x^y);	//6
	
		System.out.println(~x);		//-11
		System.out.println(~y);		//-13

		System.out.println(x<<2);	//40
		System.out.println(y<<2);	//48
		
		// values of x and y doesnt change after righ or left shift				

		System.out.println(x>>2);       //2 
		System.out.println(y>>2);       //3
	}
}
