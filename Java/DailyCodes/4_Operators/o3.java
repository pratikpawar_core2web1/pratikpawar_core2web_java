
// == , !=, > , < , >= , <= 
class Relational{

	public static void main(String ar[]){
	
		int x,y;
		x=8;
		y=8;

		System.out.println(x==y);       //true
		System.out.println(x!=y);       //false
		System.out.println(x>y);       //false 
		System.out.println(x<y);       //false 
		System.out.println(x>=y);	//true
		System.out.println(x<=y);       //true
	}
}
