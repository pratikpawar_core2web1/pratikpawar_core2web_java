
// +,-,++,--
class Unary{

	public static void main(String ar[]){
	
		int x=10;
		System.out.println(+x);		//+10
		System.out.println(-x);		//-10
		System.out.println(++x);	//11
		System.out.println(--x);	//10
		
		 x =5;
		System.out.println(x++);	//5
		System.out.println(x);		//6
		System.out.println(x--);	//6
		System.out.println(x);		//5
	}
}
