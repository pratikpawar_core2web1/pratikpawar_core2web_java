
class sdemo{
	
	public static void main(String arf[]){
		
		String str1 = "PAwar";
		String str2 = "Pawar";
		String str3 = new String( "PawAr");
		String str4 = new String( "PaWar");

		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
		System.out.println(System.identityHashCode(str3));
		System.out.println(System.identityHashCode(str4));
	}
}
