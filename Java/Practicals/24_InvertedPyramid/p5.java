

import java.util.*;

class pdemo{
	
	public static void main(String arf[]){
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows : ");
		int rows = sc.nextInt();
		int tmp =rows;
		char ch = 'A';
		for(int i=1;i<=rows;i++,tmp--){
			
			for(int s=1;s<i;s++){
				
				System.out.print("\t");
			}

			for(int j=1;j<=tmp*2-1;j++){
				
				System.out.print(ch +"\t");
			}
			ch++;
			System.out.println();
		}
	}
}
