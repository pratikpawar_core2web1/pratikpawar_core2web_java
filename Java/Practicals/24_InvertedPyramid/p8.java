

import java.util.*;

class pdemo{
	
	public static void main(String arf[]){
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows : ");
		int rows = sc.nextInt();
		int tmp =rows;
		
		for(int i=1;i<=rows;i++,tmp--){
			
			for(int s=1;s<i;s++){
				
				System.out.print("\t");
			}
			int n =1;     
			for(int j=1;j<=tmp*2-1;j++){
			
				if(j<tmp){		
					System.out.print(n++ +"\t");
				}
				else{
					System.out.print(n-- +"\t");
				}
			}
			
			System.out.println();
		}
	}
}
