

import java.util.*;

class pdemo{
	
	public static void main(String arf[]){
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows : ");
		int rows = sc.nextInt();
		int tmp =rows;
		
		for(int i=1;i<=rows;i++,tmp--){
			
			for(int s=1;s<i;s++){
				
				System.out.print("\t");
			}
			
			for(int j=tmp*2-1;j>=1;j--){
			
				if(j%2==1){		
					System.out.print("1\t");
				}
				else{
					System.out.print("0\t");
				}
			}
			
			System.out.println();
		}
	}
}
