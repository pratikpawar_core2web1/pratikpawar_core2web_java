

import java.util.*;

class ademo{
	
	public static void main(String arg[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter rows : ");
		int rows = sc.nextInt();
		

		System.out.println("Enter columns : ");
		int cols = sc.nextInt();

		int arr[][] = new int[rows][cols];

		System.out.println("Enter numbers : ");
		for(int i=0;i<rows;i++){
			
			for(int j=0;j<cols; j++){
				
				arr[i][j] = sc.nextInt();
			}
		}
		
		int sum =0;
		for(int i=0;i<rows;i++){
			
			sum += arr[i][cols-1-i];
		}		
		System.out.println("Sum of seconday diagonal = "+sum);
	}
}
