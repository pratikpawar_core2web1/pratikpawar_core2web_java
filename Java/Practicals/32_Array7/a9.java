

import java.util.*;

class ademo{
	
	public static void main(String arg[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter rows : ");
		int rows = sc.nextInt();
		

		System.out.println("Enter columns : ");
		int cols = sc.nextInt();

		int arr[][] = new int[rows][cols];

		System.out.println("Enter numbers : ");
		for(int i=0;i<rows;i++){
			
			for(int j=0;j<cols; j++){
				
				arr[i][j] = sc.nextInt();
			}
		}
		
		int sum1 =0;
		int sum2 =0;
		for(int i=0;i<rows;i++){
			
			sum1 += arr[i][i];
			sum2 += arr[i][cols-1-i];
		}		
		System.out.println("prod of Sum of primary and seconday diagonal = "+(sum1*sum2));
	}
}
