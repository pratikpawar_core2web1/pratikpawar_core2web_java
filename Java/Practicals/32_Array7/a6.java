

import java.util.*;

class ademo{
	
	public static void main(String arg[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter rows : ");
		int rows = sc.nextInt();
		

		System.out.println("Enter columns : ");
		int cols = sc.nextInt();

		int arr[][] = new int[rows][cols];


		System.out.println("Enter numbers : ");
		for(int i=0;i<rows;i++){
			
			for(int j=0;j<cols; j++){
				
				arr[i][j] = sc.nextInt();
			}
		}
		
		for(int i=0;i<rows;i++){
			
			for(int j=0;j<cols; j++){
				
				if(arr[i][j] % 3 == 0){
					
					System.out.print(arr[i][j]+ ",");
				}
			}
		}

		System.out.println();
	}
}
