

import java.util.*;

class ardemo{
	 
	public static void main(String arg[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter elements : ");
		for(int i=0;i<arr.length;i++){
				
			arr[i] = sc.nextInt();
		}
		int cnt =0;
		System.out.print("Composite numbers in an array : ");
		for(int i=0;i<size;i++){
			cnt=0;
			for(int j=1;j<=arr[i];j++){
				
				if(arr[i]%j==0){
					cnt++;
				}
			}
			if(cnt>2){
				System.out.print(arr[i]+",");
			}
		}

		System.out.println();
		
	}
}
	
