

import java.util.*;

class ardemo{
	 
	public static void main(String arg[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter elements : ");
		for(int i=0;i<arr.length;i++){
				
			arr[i] = sc.nextInt();
		}
		System.out.println();
		int n =0;
		int rem =0;
		int cnt =0;
		for(int i=0;i<arr.length;i++){
			
			n = arr[i];
			rem=0;
			cnt =0;
			while(n!=0){
				
				rem = n%10;
				n /= 10;
				cnt++;
			}
			System.out.print(cnt+",");
		}
		
		System.out.println();
	}
}
	
