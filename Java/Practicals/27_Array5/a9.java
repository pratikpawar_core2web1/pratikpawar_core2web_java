

import java.util.*;

class ardemo{
	 
	public static void main(String arg[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter number : ");
		int num = sc.nextInt();


		int rem=0;
		int rev =0;
		int n=num;
		int cnt =0;
		while(n!=0){
			
			rem = n%10;
			rev = rev*10+rem;
			n/=10;
			cnt++;
		}
		
		int arr[] = new int[cnt];

		num=rev;
		rem=0;
		int i=0;
		while(num!=0){
			rem = num%10;
			arr[i] = rem+1;
			num/=10;
			i++;
		}

		for(int j=0;j<arr.length;j++){
			
			System.out.print(arr[j]+",");
		}
		System.out.println();
	}
}
	
