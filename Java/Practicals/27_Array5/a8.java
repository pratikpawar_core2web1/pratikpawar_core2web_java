

import java.util.*;

class ardemo{
	 
	public static void main(String arg[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter elements : ");
		for(int i=0;i<arr.length;i++){
				
			arr[i] = sc.nextInt();
		}
		
		int min = arr[0];
		int min2 = arr[1];

		int temp=0;
		if(min>min2){
			temp = min;
			min = min2;
			min2 = temp;
		}

		for(int i=2;i<size;i++){
			
			if(arr[i]<min){
				
				min2 = min;
				min = arr[i];
			}
			else if(arr[i]<min2 && arr[i]>min){
				
				min2 = arr[i];
			}
		}

		System.out.println("Second minimum element in arry is "+min2);
	}
}
	
