

import java.util.*;

class ardemo{
	 
	public static void main(String arg[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Emter size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		int esum =0;
		int osum =0;
		
		System.out.println("Enter elements : ");
		
		for(int i=0;i<arr.length;i++){
				
			arr[i] = sc.nextInt();

			if(arr[i]%2==0){
				
				esum += arr[i];
			}
			else{
				osum += arr[i];
			}
		}

		System.out.println("Sum of even numbers : "+esum);
		System.out.println("Sum of odd numbers : "+osum);
		
	}
}
	
