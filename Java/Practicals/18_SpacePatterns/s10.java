

import java.util.*;

class sdemo{
	
	public static void main(String arh[]){
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows = ");
		int rows = sc.nextInt();
		
		
		for(int i=1;i<=rows ;i++){
			int cnt = 1;
			for(int s=1;s<i;s++){
				
				System.out.print("  " );
				cnt++;
			}
			int n = cnt+64;
			for(int j=rows;j>=i;j--){
				
				if(n%2==1){
					System.out.print(n + " ");
				}
				else{
					System.out.print((char)n + " ");
				
				}
				n++;
			}
			
			System.out.println();
		}

	}
}
