

import java.util.*;

class sdemo{
	
	public static void main(String arh[]){
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows = ");
		int rows = sc.nextInt();

		for(int i=1;i<=rows ;i++){
			
			int cnt = 1;
			for(int s=rows;s>i;s--){
				
				System.out.print("  " );
				cnt++;
			}
			
			int ch = cnt +64;
			for(int j=1;j<=i;j++){
				
				System.out.print((char)ch++ + " ");
			}
			System.out.println();
		}

	}
}
