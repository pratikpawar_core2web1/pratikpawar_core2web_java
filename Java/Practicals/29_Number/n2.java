
import java.util.*;

class ndemo{
	
	public static void main(String ar[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter number : ");
		int n = sc.nextInt();
		
		int num =n;
		int rem =0;
		int sum =0;

		while(num!=0){
			
			rem = num%10;
			int fact = 1;
			while(rem > 1){
				
				fact *= rem;
				rem--;
			}

			sum += fact;
			num /= 10;
		}
	
		if(sum == n)
			System.out.println(n+" is Strong  number!!");
		else
			System.out.println(n+" is not  Strong number!!");
	}
}
