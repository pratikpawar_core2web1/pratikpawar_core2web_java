
import java.util.*;

class ndemo{
	
	public static void main(String ar[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter number : ");
		int n = sc.nextInt();
		
		int cnt=0;
		int rem =0;
		int num =n;

		while(num!=0){
			
			rem = num%10;
			num/=10;
			cnt++;
		}

		num = n;
		rem =0;
		int sum =0;
		while(num!=0){
				
			rem = num%10;
			int prod =1;
			for(int i=0;i<cnt;i++){
				prod *= rem;		
			}
			sum += prod;
			num /=10;
		}

		if(n==sum)
			System.out.println(n+" is Armstrong number");
		else
			System.out.println(n+" is not  Armstrong number");

	}
}
