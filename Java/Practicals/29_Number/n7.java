
import java.util.*;

class ndemo{
	
	static int check(int n){
		
		int rem=0;
		int sqr = 0;
		int sum =0;
		while(n!=0){
			
			rem = n%10;
			sqr = rem*rem;
			sum += sqr;
			n /=10;
		}
		return sum;
	}
	public static void main(String ar[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter number : ");
		int n = sc.nextInt();
		int result = n;
		while(result!=1 && result!=4){
			
			result = check(result);
		}

		if(result == 1)
			System.out.println(n +" is happy number ");
		else
			System.out.println(n +" is not happy number ");
	}
}
