
import java.util.*;

class ndemo{
	
	public static void main(String ar[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter number : ");
		int n = sc.nextInt();
		
		int cnt =0;
		int num = n;
		int rem = 0;
		while(num!=0){
			
			rem = num%10;
			num /= 10;
			cnt++;
		}
	       
		int sqr = n*n;

		int rev1 =0;
		rem  = 0;
		num = sqr;

		while(cnt !=0){
			
			rem = num%10;
			rev1 = rev1*10 + rem;
			num /=10;
			cnt--;
		}
		
		int rev2 =0;
		rem  = 0;
		num = rev1;

		while(num !=0){
			
			rem = num%10;
			rev2 = rev2*10 + rem;
			num /=10;
		}

		if(rev2 == n){
			System.out.println(n+" is Automorphic number !!");
		}
		else
			System.out.println(n+" is not Automorphic number !!");

	}
}
