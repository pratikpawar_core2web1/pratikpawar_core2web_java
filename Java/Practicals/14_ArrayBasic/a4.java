
import java.util.*;
class ademo{

	public static void main(String arg[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		
		for(int i=0;i<arr.length;i++){
			System.out.print("Enter no "+i+" :");
			arr[i] = sc.nextInt();
		}
	
		int sum =0;
		for(int i=0;i<arr.length;i++){
			
			if(arr[i]%2==1){
				
				sum+= arr[i];
				
			}
		}
		System.out.println("Sum : "+sum);
	}
}
