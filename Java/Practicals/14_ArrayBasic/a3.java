
import java.util.*;
class ademo{

	public static void main(String arg[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		
		for(int i=0;i<arr.length;i++){
			System.out.print("Enter no "+i+" :");
			arr[i] = sc.nextInt();
		}

		System.out.print("Array elements : ");

		for(int i=0;i<arr.length;i++){
			
			if(arr[i]%2==0){
				System.out.print(arr[i]+ " ,");
			}
		}
		System.out.println();
	}
}
