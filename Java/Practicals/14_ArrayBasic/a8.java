
import java.util.*;
class ademo{

	public static void main(String arg[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter count of employees : ");
		int cnt = sc.nextInt();

		int age[] = new int[cnt];
		
		for(int i=0;i<age.length;i++){
			System.out.print("Enter age of emp "+i+" :");
			age[i] = sc.nextInt();
		}

		System.out.print("Ages : ");

		for(int i=0;i<age.length;i++){
			
			System.out.print(age[i]+ " ,");
		}
		System.out.println();
	}
}
