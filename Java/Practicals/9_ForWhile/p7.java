

class w2demo{
	
	public static void main(String arg[]){
		
		int n = 256985;
		int rem = 0;
		int sum = 0;
		   
		while(n!=0){
			
			rem = n%10;
			
			if(rem%2==0){
				
				sum += rem;
			}

			n = n/10;
		}
		System.out.println("Sum of even digits : "+sum);
	}
}
