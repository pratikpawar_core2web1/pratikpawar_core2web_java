

class w2demo{
	
	public static void main(String arg[]){
		
		int n = 9367924;
		int rem = 0;
		int prod= 1;
		int sum = 0;
		while(n!=0){
			
			rem = n%10;
			
			if(rem%2==1){
				
				sum += rem;
				
			}
			else{
			
				prod *= rem;

			}

			n = n/10;
		}
		System.out.println("Sum of odd digits : "+sum);
		System.out.println("Product of even digits : "+prod);
	}
}
