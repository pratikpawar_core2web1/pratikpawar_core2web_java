

class w2demo{
	
	public static void main(String arg[]){
		
		int n = 2469185;
		int rem = 0;
		int sqr;
		int sum = 0;
		while(n!=0){
			
			rem = n%10;
			
			if(rem%2==1){
				
				sqr = rem*rem;
				sum += sqr;
				
			}

			n = n/10;
		}
		System.out.println(sum);
	}
}
