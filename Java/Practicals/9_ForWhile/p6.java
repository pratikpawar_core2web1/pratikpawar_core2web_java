

class w2demo{
	
	public static void main(String arg[]){
		
		int n = 234;
		int rem = 0;
		int prod = 1;
		
		while(n!=0){
			
			rem = n%10;
			prod *= rem;
			n = n/10;
		}
		System.out.println(prod);
	}
}
