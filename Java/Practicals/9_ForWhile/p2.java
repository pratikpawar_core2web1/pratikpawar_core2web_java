

class w2demo{
	
	public static void main(String arg[]){
		
		int n = 2569185;
		int rem = 0;

		System.out.print("digits not  divisible by 3 are : ");   
		while(n!=0){
			
			rem = n%10;
			
			if(rem%3!=0){
				
				System.out.print(rem + " ");
			}

			n = n/10;
		}
		System.out.println();
	}
}
