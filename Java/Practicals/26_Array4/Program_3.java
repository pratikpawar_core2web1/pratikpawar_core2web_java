import java.util.Scanner;

class Program_3 {
	public  static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Size of Array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array Data: ");
		for(int i=0;i<arr.length;i++){
			arr[i] =sc.nextInt();
		}

		System.out.print("Array Data: ");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i] +", ");
		}
		System.out.println();

		int max = arr[0];
		int secMax = arr[1];

		if(max<secMax){
			int temp = max;
			max = secMax ;
			secMax = temp;
		}

		for(int i=2;i<arr.length;i++){
			if(arr[i]>max){
				secMax = max;
				max = arr[i];

			}else if(arr[i]>secMax && arr[i]!=max){
				secMax = arr[i];
			}
		}

		System.out.println("Second Max Element in Array is : "+secMax);
	}
}
