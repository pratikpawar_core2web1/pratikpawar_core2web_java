import java.util.Scanner;

class Program_1 {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Elements of array: ");
		for(int i=0;i<arr.length;i++){

			arr[i] = sc.nextInt();

		}

		System.out.print("Array Data: ");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i] +" ");
		}
		System.out.println();
		
		int sum=0;

		for(int i=0;i<arr.length;i++){

			sum=sum+arr[i];
		}
		int avg = sum/arr.length;

		System.out.println("Average of Array Elements:  "+avg);
	}
}
