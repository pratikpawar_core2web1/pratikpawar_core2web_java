import java.util.Scanner;

class Program_6 {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of array: ");
		int size = sc.nextInt();

		char arr[] = new char[size];

		System.out.println("Enter Elements of array: ");
		for(int i=0;i<arr.length;i++){

			arr[i] = sc.next().charAt(0);

		}

		System.out.print("Array Data: ");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i] +" ");
		}
		System.out.println();

		int vowCnt=0;
		int conCnt=0;

		for(int i=0;i<arr.length;i++){

			if(arr[i]=='a' || arr[i]=='A' || arr[i]=='e' || arr[i]=='E' || arr[i]=='i' || arr[i]=='I'
				       	 || arr[i]=='o' || arr[i]=='O' || arr[i]=='u' || arr[i]=='U' ){

			
				vowCnt++;

					 }else{
						 conCnt++;
					 }
		}
		System.out.println("Vowels Count: "+vowCnt);
		System.out.println("Consonant Count: "+conCnt);
	}
}

