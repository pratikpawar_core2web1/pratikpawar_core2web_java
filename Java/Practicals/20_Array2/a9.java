

import java.util.*;

class ademo{
	
	public static void main(String ar[]){
	
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter size of array : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.print("Enter elements : ");
		for(int i=0;i<arr.length;i++){
		
			arr[i]= sc.nextInt();
		}
		
		int min = arr[0];
		for(int i=0;i<arr.length;i++){
			
			if(arr[i]<min){
				
				min = arr[i];	
			}
		}
		System.out.println(min + " is minumum number of array ");
	}
}
