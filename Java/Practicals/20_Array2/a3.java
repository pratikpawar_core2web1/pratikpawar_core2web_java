

import java.util.*;

class ademo{
	
	public static void main(String ar[]){
	
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter size of array : ");
		int size = sc.nextInt();

		char arr[] = new char[size];

		System.out.print("Enter elements : ");
		for(int i=0;i<arr.length;i++){
		
			arr[i]= sc.next().charAt(0);
		}
		
		
		System.out.println();
		for(int i=0;i<arr.length;i++){
			
			if(arr[i] == 'A'|| arr[i] == 'a' || arr[i] == 'E'|| arr[i] == 'e'|| arr[i] == 'I'|| arr[i] == 'i'|| arr[i] == 'O'|| arr[i] == 'o'|| arr[i] == 'U'|| arr[i] == 'u'){
				System.out.println("Vowel "+arr[i]+ " fount at index "+ i);
			}
			
		}
	}
}
