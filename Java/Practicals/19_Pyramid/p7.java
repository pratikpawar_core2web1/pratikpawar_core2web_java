

import java.util.*;

class pdemo{
	
	public static void main(String arg[]){
		
		Scanner sc = new Scanner(System.in);
	
		System.out.print("Enter rows : ");
		int rows = sc.nextInt();
		
		
		for(int i=1;i<=rows;i++){
			
			for(int s=rows ; s>i; s--){
				
				System.out.print("  ");
	
			}
			
			for(int j=1;j<=i*2-1;j++){
				if(i%2==1){
				    System.out.print(i + " ");
			    }
			    else{
			        int ch = i+64;
			        System.out.print((char)ch + " ");
			    }
			}
			System.out.println();
		}
	}
}