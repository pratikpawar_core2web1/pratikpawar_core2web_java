

import java.util.*;

class pdemo{
	
	public static void main(String arg[]){
		
		Scanner sc = new Scanner(System.in);
	
		System.out.print("Enter rows : ");
		int rows = sc.nextInt();
		
		
		for(int i=1;i<=rows;i++){
			
			for(int s=rows ; s>i; s--){
				
				System.out.print("  ");
	
			}
			int n =1;
			for(int j=1;j<=i*2-1;j++){
				if(j<i){
				    System.out.print(n++ + " ");
			    }
			    else{
			        System.out.print(n-- + " ");
			    }
			}
			System.out.println();
		}
	}
}