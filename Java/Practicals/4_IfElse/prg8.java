

class p8{
	
	public static void main(String ar[]){
		
		float per = 28.60f;

		if(per>85.00){
			
			System.out.println("Passed : Distinction");
		}
		else if(per <= 85.00 && per >75.00){
			
			System.out.println("Passed : First class");
		}
		else if(per <= 75.00 && per >65.00){
			
			System.out.println("Passed : Second class");
		}
		else if(per <= 65.00 && per >34){
			System.out.println("Passed : Third class ");
		}
		else{
			System.out.println("Failed!!");
		}
	}
}
