
import java.util.*;
class pdemo{
	
	public static void main(String arg[]){
		
		Scanner sc= new Scanner(System.in);
		
		System.out.print("Enter rows : ");
		int rows = sc.nextInt();
		
	
		for(int i=1;i<=rows;i++){
			int n = 1;
			for(int j=1;j<=rows; j++){
				
				if(i%2==1){
					if(j%2==1){
					
						System.out.print(2*n++ + " ");
					}
					else{
						System.out.print(3*n++ + " ");
					}
				}
				else{
					if(j%2==1){
					
						System.out.print(3*n++ + " ");
					}
					else{
						System.out.print(2*n++ + " ");
					}
				}
			}
			System.out.println();
		}
	}
}
