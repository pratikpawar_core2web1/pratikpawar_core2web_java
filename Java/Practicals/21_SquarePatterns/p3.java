
import java.util.*;
class pdemo{
	
	public static void main(String arg[]){
		
		Scanner sc= new Scanner(System.in);
		
		System.out.print("Enter rows : ");
		int rows = sc.nextInt();
		
		int n=rows;
		for(int i=1;i<=rows;i++){
			
			for(int j=1;j<=rows; j++){
				
				if(n%rows==0){
					
					System.out.print(n*n + " ");
				}
				else{
					System.out.print(n + " ");
				}
				n++;
			}
			System.out.println();
		}
	}
}
