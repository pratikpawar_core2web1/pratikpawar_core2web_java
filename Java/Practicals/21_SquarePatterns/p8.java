
import java.util.*;
class pdemo{
	
	public static void main(String arg[]){
		
		Scanner sc= new Scanner(System.in);
		
		System.out.print("Enter rows : ");
		int rows = sc.nextInt();
		
	
		for(int i=1;i<=rows;i++){
			int ch = rows+64;
			for(int j=1;j<=rows; j++){
				
				if(i%2==1){
					if(j%2==0){
					
						System.out.print((char)ch-- + " ");
					}
					else{
						System.out.print("# ");
					}
				}
				else{
					if(j%2==1){
					
						System.out.print((char)ch-- + " ");
					}
					else{
						System.out.print("# ");
					}
				}
			}
			System.out.println();
		}
	}
}
