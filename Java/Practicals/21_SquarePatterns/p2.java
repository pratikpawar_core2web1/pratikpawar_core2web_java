
import java.util.*;
class pdemo{
	
	public static void main(String arg[]){
		
		Scanner sc= new Scanner(System.in);
		
		System.out.print("Enter rows : ");
		int rows = sc.nextInt();
		
		int n=rows;
		for(int i=1;i<=rows;i++){
			
			for(int j=1;j<=rows; j++){
				
				if(n%3==0){
					
					System.out.print(3*n + " ");
				}
				else if(n%5==0){
					
					System.out.print(5*n + " ");
				}
				else{
					System.out.print(n + " ");
				}
				n++;
			}
			System.out.println();
		}
	}
}
