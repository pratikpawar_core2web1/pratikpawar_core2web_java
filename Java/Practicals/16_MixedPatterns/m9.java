
import java.util.*;

class mdemo{
	
	public static void main(String atr[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows = ");
		int rows = sc.nextInt();
		int temp = rows;
		 
		
		for(int i=1;i<=rows ;i++,temp--){
			int n = 1;
			int ch = temp+64;
			for(int j=rows;j>=i ; j--){
				
				if(i%2==0){
					System.out.print((char)ch--  +" ");
				}else{
					System.out.print(n++ + " ");
				}
				
			}
			System.out.println();
		}
	}
}
