
import java.util.*;

class mdemo{
	
	public static void main(String atr[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows = ");
		int rows = sc.nextInt();
		int temp = rows;
		
		for(int i=1;i<=rows ;i++,temp++){
			int n = 1;
			int ch = rows+64;
			for(int j=1;j<=rows ; j++){
				
				if(i%2==0){
					
					System.out.print(n++ + " ");
					
				}else{
					System.out.print((char)ch-- + " ");
			
				}
			}
			System.out.println();
		}
	}
}
