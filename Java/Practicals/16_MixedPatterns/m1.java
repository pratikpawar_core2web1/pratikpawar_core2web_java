
import java.util.*;

class mdemo{
	
	public static void main(String atr[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows = ");
		int rows = sc.nextInt();

		int n =1;

		for(int i=1;i<=rows ;i++){
			
			for(int j=1;j<=rows ; j++){
				
				System.out.print(n++ + " ");
			}
			System.out.println();
		}
	}
}
