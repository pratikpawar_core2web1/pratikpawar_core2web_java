
import java.util.*;

class mdemo{
	
	public static void main(String atr[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows = ");
		int rows = sc.nextInt();
	
		for(int i=1;i<=rows ;i++){
			int n = rows;
			int ch = rows+96;
			for(int j=1;j<=i ; j++){
				
				if(i%2==0){
					System.out.print(n-- +" ");
				}
				else{
					System.out.print((char)ch-- +" ");
				}
			}
			System.out.println();
		}
	}
}
