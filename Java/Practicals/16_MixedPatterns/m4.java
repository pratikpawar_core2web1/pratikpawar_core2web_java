
import java.util.*;

class mdemo{
	
	public static void main(String atr[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows = ");
		int rows = sc.nextInt();
		int temp = rows;
		
		for(int i=1;i<=rows ;i++,temp--){
			int n = temp;
			for(int j=1;j<=i ; j++){
				
				System.out.print(n*j +" ");
			}
			System.out.println();
		}
	}
}
