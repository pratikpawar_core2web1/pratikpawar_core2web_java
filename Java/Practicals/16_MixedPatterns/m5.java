
import java.util.*;

class mdemo{
	
	public static void main(String atr[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows = ");
		int rows = sc.nextInt();
		int temp = rows;
		
		for(int i=1;i<=rows ;i++,temp--){
			
			for(int j=1;j<=i ; j++){
				
				System.out.print(i*j +" ");
			}
			System.out.println();
		}
	}
}
