
import java.io.*;
class ndemo{
	
	public static void main(String ar[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter num = ");
		int n = Integer.parseInt(br.readLine());

		System.out.println();
		System.out.print("Factors of number : ");

		for(int i=1;i<=n;i++){
			
			if(n%i==0){
				System.out.print(i+ " , ");
			}
		}
		System.out.println();

	}
}
