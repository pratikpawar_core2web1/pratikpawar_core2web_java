
import java.io.*;
class ndemo{
	
	public static void main(String ar[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter num = ");
		int n = Integer.parseInt(br.readLine());

		System.out.println();
		int flag = 1;
		for(int i=2;i<=n/2;i++){
			
			if(n%i==0){
				flag = 0;
			}
		}
		if(flag == 1){
			
			System.out.println(n+ " is prime number");

		}else{
			System.out.println(n+ " is not a prime number");
		}
	}
}
