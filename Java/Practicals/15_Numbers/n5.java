
import java.io.*;
class ndemo{
	
	public static void main(String ar[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter num = ");
		int n = Integer.parseInt(br.readLine());

		int tmp = n;
		int rem = 0;
		int rev = 0;
		while(tmp!=0){
			
			rem = tmp%10;
			rev = rev*10 + rem;
			tmp = tmp /10;
		}


		System.out.println("Reverse of "+n+" is "+ rev);
	}
}
