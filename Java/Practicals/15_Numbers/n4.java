
import java.io.*;
class ndemo{
	
	public static void main(String ar[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter num = ");
		int n = Integer.parseInt(br.readLine());

	
		int fact = 1;
		for(int i=n;i>1;i--){
			
			fact *= i;
		}
		System.out.println("Factorial of "+n+ " is "+fact);
	}
}
