

import java.util.*;
class ademo{
	
	public static void main(String arg[]){
		
		Scanner sc = new Scanner(System.in);

		int arr[] = new int[]{1,5,9,5,7,5};
		
		System.out.print("Array : ");
		for(int i=0;i<arr.length;i++){
			
			System.out.print(arr[i] + " ");
		}
		System.out.println();


		System.out.print("Enter specific number : ");
		
		int srch = sc.nextInt();
		int cnt = 0;

		for(int i=0;i<arr.length;i++){
			
			if(arr[i] == srch){
				cnt++;
			}
		}
		if(cnt == 0){
			System.out.println("Number "+srch+" not found!!");
		}	
		else{
			System.out.println("Number "+srch+" occured "+cnt+" times in an array");
		}
	}
}
