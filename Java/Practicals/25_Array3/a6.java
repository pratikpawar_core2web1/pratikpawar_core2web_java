

import java.util.*;
class ademo{
	
	public static void main(String arg[]){
		
		Scanner sc = new Scanner(System.in);

		char arr[] = new char[]{'a','b','c','d','e','f','g'};
		
		System.out.print("Array : ");
		for(int i=0;i<arr.length;i++){
			
			System.out.print(arr[i] + " ");
		}
		System.out.println();

		for(int i=0;i<arr.length;i++){

			if(arr[i]!='a'&&arr[i]!='e'&&arr[i]!='i'&&arr[i]!='o'&&arr[i]!='u'){
				System.out.print(arr[i] + " ");
			}

		}
		System.out.println();
	}
}
