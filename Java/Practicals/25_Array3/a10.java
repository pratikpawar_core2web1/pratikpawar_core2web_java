

import java.util.*;
class ademo{
	
	public static void main(String arg[]){
		
		Scanner sc = new Scanner(System.in);

		int arr[] = new int[]{1,4,5,6,11,9,10};
		
		System.out.print("Array : ");
		for(int i=0;i<arr.length;i++){
			
			System.out.print(arr[i] + " ");
		}
		System.out.println();
		
		int prod =1;
		for(int i=0;i<arr.length;i++){
			int flag =0;
			for(int j=2;j<=arr[i]/2;j++){
				
				if(arr[i]%j==0){
					
					flag =1 ;
				}
			}
			
			if(flag==0 && arr[i]!=1){
				prod *= arr[i];
			}
		}
		System.out.println(prod);
	}
}
