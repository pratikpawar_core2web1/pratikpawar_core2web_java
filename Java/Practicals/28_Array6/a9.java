

import java.util.*;

class ademo{
	
	public static void main(String arg[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size of an array : ");
		int size = sc.nextInt();
		
		int arr[] = new int[size];
		
		int pcnt=0;
		System.out.println("Enter "+size+" numbers : ");
		for(int i=0;i<arr.length;i++){
				
			arr[i] = sc.nextInt();
			int rev=0;
			int rem =0;
			int n = arr[i];
			while(n!=0){
				
				rem = n%10;
				rev = rev*10 + rem;
				n/=10;
			}
			
			if(rev==arr[i]){
				pcnt++;	
			}
		}
		System.out.println("Count of palindrome numbers is "+pcnt);
	}
}
