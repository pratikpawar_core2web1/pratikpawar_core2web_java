

import java.util.*;

class ademo{
	
	public static void main(String arg[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size of an array : ");
		int size = sc.nextInt();
		
		int arr[] = new int[size];

		System.out.println("Enter "+size+" numbers : ");
		for(int i=0;i<arr.length;i++){
				
			arr[i] = sc.nextInt();
		}

		System.out.println("Updated array : ");

		for(int i=0;i<size;i++){
			
			if(arr[i]>=65 && arr[i]<=90){
				System.out.print((char)arr[i]+" ");
			}
			else{
				System.out.print(arr[i]+" ");
			}
		}
		System.out.println();
	}
}
