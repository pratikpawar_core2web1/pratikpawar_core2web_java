

import java.util.*;

class ademo{
	
	public static void main(String arg[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size of an array : ");
		int size = sc.nextInt();
		
		int arr[] = new int[size];
		
		int sum =0;
		int pcnt=0;
		System.out.println("Enter "+size+" numbers : ");
		for(int i=0;i<arr.length;i++){
				
			arr[i] = sc.nextInt();
			int cnt=0;
			for(int j=1;j<=arr[i]/2;j++){
				
				if(arr[i]%j==0){
					cnt++;
				}
			}
			if(cnt==1){
				sum += arr[i];
				pcnt++;	
			}
		}
		System.out.println("Sum of all prime numbers is "+sum+" and Count of prime numbers is "+pcnt);
	}
}
