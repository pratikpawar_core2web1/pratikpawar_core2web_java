

import java.util.*;

class ademo{
	
	public static void main(String arg[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size of an array : ");
		int size = sc.nextInt();
		
		int arr[] = new int[size];

		System.out.println("Enter "+size+" numbers : ");
		for(int i=0;i<arr.length;i++){
				
			arr[i] = sc.nextInt();
		}

		int flag =0;
		for(int i=1;i<size;i++){
			
			if(arr[i-1]<arr[i]){
				flag =1;
			}
		}
		if(flag ==1){
			System.out.println("Array is not in descending order !!");
		}
		else{
			System.out.println("Array is in descending order !!");
			
		}
	}
}
