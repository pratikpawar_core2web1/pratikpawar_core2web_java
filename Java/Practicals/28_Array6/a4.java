
import java.util.*;

class ademo{
	
	public static void main(String arg[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size of an array : ");
		int size = sc.nextInt();
		
		int arr1[] = new int[size];
		int arr2[] = new int[size];

		System.out.println("Enter array1 numbers : ");
		for(int i=0;i<arr1.length;i++){
				
			arr1[i] = sc.nextInt();
		}
		
		System.out.println("Enter array2 numbers : ");
		for(int i=0;i<arr2.length;i++){
				
			arr2[i] = sc.nextInt();
		}
		
		System.out.print("Common elements of both arrays are : ");
		for(int i=0;i<size;i++){
			
			for(int j=0;j<size;j++){
				
				if(arr1[i]==arr2[j]){
					System.out.print(arr1[i]+ ",");	
				}
			}
		}

		System.out.println();
	}
}
