

import java.util.*;

class ademo{
	
	public static void main(String arg[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size of an array : ");
		int size = sc.nextInt();
		
		int arr[] = new int[size];

		System.out.println("Enter "+size+" numbers : ");
		for(int i=0;i<arr.length;i++){
				
			arr[i] = sc.nextInt();
		}
		
		System.out.println("Enter key : ");
		int key = sc.nextInt();
		int cnt=0;
		for(int i=0;i<size;i++){
				
			if(key==arr[i]){
				cnt++;
			}
		}
		if(cnt>2){
			
			for(int i=0;i<size;i++){
					
				if(key==arr[i]){
					arr[i] = arr[i]*arr[i]*arr[i];
				}
			}
		}
		if(cnt==0){
			System.out.println("Element not found !!");
		}
		else{
			for(int i=0;i<arr.length;i++){
				System.out.print(arr[i]+" ");	
			}
		}
		System.out.println();
	}
}
