

import java.util.*;

class ademo{
	
	public static void main(String arg[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size of an array : ");
		int size = sc.nextInt();
		
		int arr[] = new int[size];

		System.out.println("Enter "+size+" numbers : ");
		for(int i=0;i<arr.length;i++){
				
			arr[i] = sc.nextInt();
		}

		int max = Integer.MIN_VALUE;
	        int max2 = Integer.MIN_VALUE;
		int max3 = Integer.MIN_VALUE;

		for(int i=0;i<size;i++){
			
			if(arr[i]>max){
				
				max=arr[i];
			}
		} 
		for(int i=0;i<size;i++){
			
			if(arr[i]>max2 && arr[i]<max){
				
				max2=arr[i];
			}
		}
		for(int i=0;i<size;i++){
			
			if(arr[i]>max3 && arr[i]<max2){
				
				max3=arr[i];
			}
		}
		
		System.out.println("Third largest number is "+max3);
		
	}
}
