

import java.util.*;

class ademo{
	
	public static void main(String arg[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size of an array : ");
		int size = sc.nextInt();
		
		char arr[] = new char[size];

		System.out.println("Enter "+size+" numbers : ");
		for(int i=0;i<arr.length;i++){
				
			arr[i] = sc.next().charAt(0);
		}
			
		System.out.println("Array before reverse : ");
		for(int i=0;i<size;i++){
			if(i%2==0){
				System.out.print(arr[i]+" ");
			}
		}
		System.out.println();
		for(int i=0;i<=size/2;i++){
		
			char temp = arr[i];
			arr[i] = arr[size-i-1];
			arr[size-i-1] = temp;
		}

		System.out.println("Array after reverse : ");
		for(int i=0;i<size;i++){
			if(i%2==0){
				System.out.print(arr[i]+" ");
			}
		}
		System.out.println();
	}
}
