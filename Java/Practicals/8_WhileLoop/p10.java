

class wdemo{
	
	public static void main(String ar[]){
		
		long n = 9307922405l;
		long tmp = n;		
		long rem = 0;
		int sum  = 0;
		
		while(tmp!=0){
			
			rem = tmp%10;
			sum += rem;
			tmp/=10;			
		}

		System.out.println("Sum of digits in "+n+ " is "+sum);

	}
}
