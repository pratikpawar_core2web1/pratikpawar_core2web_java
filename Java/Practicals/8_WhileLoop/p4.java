

class wdemo{
	
	public static void main(String arg[]){
		
		int start = 1;
		int end = 6;
		char ch ='A';

		int i = start;
		while(i<=end){
			
			if(i%2==0){
				System.out.print(i + " ");
			}
			else{			
				System.out.print(ch++ + " ");
			}
			i++;
		}
		System.out.println();
	}
}
