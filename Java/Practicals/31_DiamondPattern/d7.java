
import java.util.*;
class demo{

	public static void main(String arg[]){
	
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows : ");
        	int rows = sc.nextInt();
	
       
		int col1 = 0;
		int col2 = 0;
		for (int i = 1; i < rows*2; i++) {
			
			
			if (i <= rows) {
				col1 = rows-i;
			}
			else {
				col1 = i-rows;
			}
	    
			for (int sp = 1; sp <=col1; sp++) {
        	        	System.out.print("\t");
			}
            
        	    	if (i <= rows) {
				col2 = i*2-1;
			}
			else {
				col2-=2;
			}
			int n=65;
			for (int j = 1; j <=col2; j++) {
			
        	    		if (j <= col2/2){
					System.out.print((char)n++ + "\t");
				}
				else {
					System.out.print((char)n-- + "\t");
				}
			}
			System.out.println();
		}
	}
}


