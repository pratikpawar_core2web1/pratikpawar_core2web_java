

import java.util.*;

class sdemo{
	
	public static void main(String arf[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows : ");
		int rows = sc.nextInt();
		
		int n = rows*rows;
		for(int i=1;i<=rows;i++){
			
			for(int j=1;j<=rows;j++){
				
				if(j%2==1){
					System.out.print(n*i+ "\t");
				}
				else{
					System.out.print("@\t");			
				}	
				n--;
			}

			System.out.println();
		}
	}
}
