

import java.util.*;

class sdemo{
	
	public static void main(String arf[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows : ");
		int rows = sc.nextInt();
		
		int n = rows;
		for(int i=1;i<=rows;i++){
			
			for(int j=1;j<=rows;j++){
				
				if(i%2==1){
					if(j%2==1){
						System.out.print(n*n-1 + "\t");
					}
					else{
						System.out.print(n*n + "\t");	
					}
					n++;	
				}
				else{
					
					System.out.print(n*n + "\t");		
					n++;
				}
			}

		
			System.out.println();
		}
	}
}
