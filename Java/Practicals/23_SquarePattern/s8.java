

import java.util.*;

class sdemo{
	
	public static void main(String arf[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows : ");
		int rows = sc.nextInt();
		
		int n = rows;
		int ch = rows+97;
		for(int i=1;i<=rows;i++){
			
			for(int j=1;j<=rows;j++){
				
				if(i%2==1){
					if(j%2==1){
						System.out.print(n*n-1 + "\t");
					}
					else{
						System.out.print((char)ch+ "\t");		
					}
					n++;
					ch++;	
				}
				else{
					
					System.out.print((char)ch++ + "\t");	
					n++;
				}
			}

		
			System.out.println();
		}
	}
}
