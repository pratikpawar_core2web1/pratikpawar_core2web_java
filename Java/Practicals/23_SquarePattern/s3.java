

import java.util.*;

class sdemo{
	
	public static void main(String arf[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows : ");
		int rows = sc.nextInt();
		
		int n = rows;
		for(int i=1;i<=rows;i++){
			
			for(int j=1;j<=rows;j++){
				
				if(i%2==1){
					int ch = n+96;
					if(j%2==1){
						System.out.print((char)ch++ + "\t");
						n++;
					}
					else{
						System.out.print(n++ + "\t");
						ch++;
					}

				}
				else{
					int ch = n+96;
					if(j%2==0){
						System.out.print((char)ch++ + "\t");
						n++;
					}
					else{
						System.out.print(n++ + "\t");
						ch++;
					}	
				}
				}

		
			System.out.println();
		}
	}
	}
