

class wdemo{
	
	public static void main(String ar[]){
		
		char ch = 65;

		while(ch <= 90){
			
			if(ch == 'A' || ch == 'E'|| ch == 'I'|| ch == 'O'|| ch == 'U'){
				
				ch++;
				continue;
				
			}
			System.out.print(ch++ + " ");
		}

		System.out.println();
	}
}
