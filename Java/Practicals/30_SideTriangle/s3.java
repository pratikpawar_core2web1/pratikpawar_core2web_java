

import java.util.*;

class stdemo{
	
	public static void main(String arg[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows : ");
		int rows = sc.nextInt();
		
		System.out.println();
		int col = 0;

		for(int i=1;i<rows*2;i++){
			
			if(i<=rows){
				
				col = i;
			}
			else{
				col = rows *2-i;
			}
			int k =col;
			for(int j=1;j<=col;j++){
				
				System.out.print(k-- +"\t");
			}

			System.out.println();
		}
	}
}
