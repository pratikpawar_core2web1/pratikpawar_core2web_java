

import java.util.*;

class stdemo{
	
	public static void main(String arg[]){
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows : ");
		int rows = sc.nextInt();
		
		System.out.println();
		int col = 0;
		int k =(64+rows)+1;
		for(int i=1;i<rows*2;i++){
			
			if(i<=rows){
				
				col = i;
				k--;
			}
			else{
				col = rows *2-i;
				k++;
			}
			
			for(int j=1;j<=col;j++){
				
				System.out.print((char)k+"\t");
			}

			System.out.println();
		}
	}
}
