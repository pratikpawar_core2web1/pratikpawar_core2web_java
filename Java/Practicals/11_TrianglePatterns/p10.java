
import java.io.*;
class pdemo{


	public static void main(String arg[])throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter rows : ");
		int rows = Integer.parseInt(br.readLine());
		int dec = rows;
		
		
		for (int i=1;i<=rows;i++,dec--){
			
			int n = i;
			for(int j=dec;j>=1;j--){
				
				System.out.print(n++ +" ");
				
			}
			System.out.println();
		}
	}
}
